package Lukasz.Michalak;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class PrzedmiotTest {

    private Przedmiot przedmiot;
    Student student1;

    @Before
    public void setup(){
        przedmiot = new Przedmiot("matma",2);
        student1=new Student("lukasz","michalak",1234,3);

    }

    @Test
    public void testDodajStudenta(){
        przedmiot.dodajStudenta(student1);
        assertThat(przedmiot.getListaStudentow().size()).isEqualTo(1);

    }

    @Test
    public void testUsunStudenta(){
        przedmiot.getListaStudentow().add(student1);
        przedmiot.usunStudenta(student1);
        assertThat(przedmiot.getListaStudentow().isEmpty()).isTrue();

    }
    @Test
    public void testUsunStudentaPoPeselu(){
        przedmiot.getListaStudentow().add(student1);
        przedmiot.usunStudentaPoPeselu(1234);
        assertThat(przedmiot.getListaStudentow().isEmpty()).isTrue();
    }
    @Test
    public void usunWykladowce(){
        przedmiot.setWykladowca(new Wykladowca("jan","kowalski",123,"BOiKD","wykladowca","dr inz."));
        przedmiot.usunWykladowce();
        assertThat(przedmiot.getWykladowca()).isNull();

    }


}
