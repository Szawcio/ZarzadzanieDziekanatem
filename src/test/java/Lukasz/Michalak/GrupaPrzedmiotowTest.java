package Lukasz.Michalak;


import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class GrupaPrzedmiotowTest {

    private GrupaPrzedmiotow grupaPrzedmiotow;
    private Przedmiot p1;

    @Before
    public void setup(){
        grupaPrzedmiotow=new GrupaPrzedmiotow("Scisle");
        p1 = new Przedmiot("matma",2);

    }
    @Test
    public void testDodajPrzedmiotDoListy(){
        grupaPrzedmiotow.dodajPrzedmiotDoListy(p1);
        assertThat(grupaPrzedmiotow.getListaPrzedmiotow().contains(p1)).isTrue();

    }
    @Test
    public void testUsunPrzedmiotZListy(){
        grupaPrzedmiotow.getListaPrzedmiotow().add(p1);
        grupaPrzedmiotow.usunPrzedmiotZListy(p1);
        assertThat(grupaPrzedmiotow.getListaPrzedmiotow().isEmpty()).isTrue();
    }
    @Test
    public void testUsunPrzedmiotPoNazwie(){
        grupaPrzedmiotow.getListaPrzedmiotow().add(p1);
        grupaPrzedmiotow.usunPrzedmiotPoNazwie("matma");
        assertThat(grupaPrzedmiotow.getListaPrzedmiotow().isEmpty()).isTrue();
    }

}
