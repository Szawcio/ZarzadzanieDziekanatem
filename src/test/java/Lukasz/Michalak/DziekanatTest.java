package Lukasz.Michalak;


import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.Assert.assertEquals;


public class DziekanatTest {
    private Dziekanat dziekanat;
    private GrupaPrzedmiotow grupaPrzedmiotow;
    private Przedmiot przedmiot;
    private Wykladowca wykladowca;
    private Student student;

    @Before
    public void setup() {
        dziekanat = new Dziekanat(101);
        grupaPrzedmiotow = new GrupaPrzedmiotow("scisle");
        przedmiot = new Przedmiot("Matematyka", 5);
        wykladowca = new Wykladowca("Aleksander", "Chory", 222, "BOiKD", "Wykladowca", "dr");
        student = new Student("lukasz", "michalak", 333, 2);


    }

    @Test
    public void testDodajGrupeDoListy() {
        dziekanat.dodajGrupeDoListy(grupaPrzedmiotow);
        assertThat(dziekanat.getListaGrupPrzedmiotow().contains(grupaPrzedmiotow)).isTrue();
    }

    @Test
    public void testUsunGrupePrzedmiotowZListy() {
        dziekanat.getListaGrupPrzedmiotow().add(grupaPrzedmiotow);
        dziekanat.usunGrupePrzedmiotowZListy(grupaPrzedmiotow);
        assertThat(dziekanat.getListaGrupPrzedmiotow().size()).isEqualTo(0);
    }

    @Test
    public void testUsunGrupePrzedmiotowPoIndentyfikatorze() {
        dziekanat.getListaGrupPrzedmiotow().add(grupaPrzedmiotow);
        dziekanat.usunGrupePrzedmiotowPoIndetyfikatorze("scisle");
        assertThat(dziekanat.getListaGrupPrzedmiotow().size()).isEqualTo(0);

    }

    @Test
    public void testZnajdzStudentaIZwrocDane() {
        przedmiot.getListaStudentow().add(student);
        przedmiot.setWykladowca(wykladowca);
        grupaPrzedmiotow.dodajPrzedmiotDoListy(przedmiot);
        dziekanat.dodajGrupeDoListy(grupaPrzedmiotow);

        assertEquals("lukasz michalak pesel: 333", dziekanat.znajdzStudentaIZwrocDane(333));
    }

    @Test
    public void testZnajdzProwadzacegoIZwrocDane() {
        przedmiot.getListaStudentow().add(student);
        przedmiot.setWykladowca(wykladowca);
        grupaPrzedmiotow.dodajPrzedmiotDoListy(przedmiot);
        dziekanat.dodajGrupeDoListy(grupaPrzedmiotow);
        dziekanat.znajdzProwadzajcegoIZwrocDane(222);
        assertEquals("dr Aleksander Chory pesel: 222", dziekanat.znajdzProwadzajcegoIZwrocDane(222));


    }
}
