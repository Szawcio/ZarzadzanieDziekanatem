package Lukasz.Michalak;


public class Student {
    private String imie;
    private String nazwisko;
    private int pesel;
    private int rokStudiow;


    public Student(String imie, String nazwisko, int pesel, int rokStudiow) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.pesel = pesel;
        this.rokStudiow = rokStudiow;
    }

    public String getImie() {
        return imie;
    }

    @Override
    public String toString() {
        return "Student:" +
                " " + imie +
                " " + nazwisko +
                ", pesel: " + pesel +
                ", aktualnie na roku: " + rokStudiow;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public int getPesel() {
        return pesel;
    }

    public void setPesel(int pesel) {
        this.pesel = pesel;
    }

    public int getRokStudiow() {
        return rokStudiow;
    }

    public void setRokStudiow(int rokStudiow) {
        this.rokStudiow = rokStudiow;
    }
}
