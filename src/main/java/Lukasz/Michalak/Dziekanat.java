package Lukasz.Michalak;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static java.util.Comparator.comparing;

public class Dziekanat {
    private int npPokoju;
    private List<GrupaPrzedmiotow> listaGrupPrzedmiotow;

    public Dziekanat(int npPokoju) {
        this.npPokoju = npPokoju;
        this.listaGrupPrzedmiotow = new ArrayList<GrupaPrzedmiotow>();
    }


    public void dodajGrupeDoListy(GrupaPrzedmiotow grupaPrzedmiotow) {
        listaGrupPrzedmiotow.add(grupaPrzedmiotow);
    }

    public void usunGrupePrzedmiotowZListy(GrupaPrzedmiotow grupaPrzedmiotow) {
        listaGrupPrzedmiotow.remove(grupaPrzedmiotow);
    }

    public void usunGrupePrzedmiotowPoIndetyfikatorze(String identyfikator) {
        boolean znaleziono = false;
        Iterator<GrupaPrzedmiotow> iterator = listaGrupPrzedmiotow.iterator();
        while (iterator.hasNext()) {
            GrupaPrzedmiotow grupa = iterator.next();
            if (identyfikator.equals(grupa.getIdentyfikator())) {
                System.out.println("Grupa przedmiotow o identyfikatorze " + grupa.getIdentyfikator() + " została usunięta");
                iterator.remove();
                znaleziono = true;
            }
        }
        if (znaleziono == false) {
            System.out.println("Nie znaleziono grupy przedmiotow o podanym identyfikatorze");
        }
    }

    public void wyswietlListyStudentowDanegoPrzedmiotuIGrupyAlf(String identyfikatorGrupy, String nazwaPrzedmiotu) {
        boolean znalezionyPrzedmiot = false;
        boolean znalezionaGrupaPrzedmiotow = false;
        for (GrupaPrzedmiotow grupaPrzedmiotow : listaGrupPrzedmiotow) {
            if (identyfikatorGrupy.equals(grupaPrzedmiotow.getIdentyfikator())) {
                znalezionaGrupaPrzedmiotow = true;
                for (Przedmiot przedmiot : grupaPrzedmiotow.getListaPrzedmiotow()) {
                    if (nazwaPrzedmiotu.equals(przedmiot.getNazwa())) {
                        znalezionyPrzedmiot = true;
                        List<Student> tymczasowa = new ArrayList<>();
                        tymczasowa.addAll(przedmiot.getListaStudentow());
                        System.out.println("W grupie: " + grupaPrzedmiotow.getIdentyfikator());
                        System.out.println("Znaleziono przedmiot: " + przedmiot.getNazwa());
                        System.out.println("");
                        System.out.println("Studenci na liscie:");
                        System.out.println("");
                        tymczasowa.sort(comparing(Student::getNazwisko));
                        for (Student student : tymczasowa) {
                            System.out.println(student.toString());
                        }
                    }
                }
            }
        }
        if (!znalezionaGrupaPrzedmiotow) {
            System.out.println("Nie znaleziono grupy przedmiotow");
        }
        if (!znalezionyPrzedmiot) {
            System.out.println("Nie znaleziono przedmiotu!");
        }
    }

    public String znajdzStudentaIZwrocDane(int pesel) {
        String daneStudenta = "";

        for (GrupaPrzedmiotow grupaPrzedmiotow : listaGrupPrzedmiotow) {
            for (Przedmiot przedmiot : grupaPrzedmiotow.getListaPrzedmiotow()) {
                for (Student student : przedmiot.getListaStudentow()) {
                    if (pesel == student.getPesel()) {
                        daneStudenta = student.getImie() + " " + student.getNazwisko() + " pesel: " + student.getPesel();
                    }
                }
            }
        }
        return daneStudenta;
    }

    public String znajdzProwadzajcegoIZwrocDane(int pesel) {
        String daneWykladowcy = "";
        for (GrupaPrzedmiotow grupaPrzedmiotow : listaGrupPrzedmiotow) {
            for (Przedmiot przedmiot : grupaPrzedmiotow.getListaPrzedmiotow()) {
                if (pesel == przedmiot.getWykladowca().getPesel()) {
                    daneWykladowcy = przedmiot.getWykladowca().getStopien() + " " + przedmiot.getWykladowca().getImie() + " " + przedmiot.getWykladowca().getNazwisko() + " pesel: " + przedmiot.getWykladowca().getPesel();

                }
            }
        }
        return daneWykladowcy;

    }

    public void pokazPrzedmiotyProwadzonePrzez(int pesel){
        boolean znalezionoProwadzacego=false;
        System.out.println("Prowadzacy: "+znajdzProwadzajcegoIZwrocDane(pesel));
        System.out.println("");
        for (GrupaPrzedmiotow grupaPrzedmiotow : listaGrupPrzedmiotow) {
            for (Przedmiot przedmiot : grupaPrzedmiotow.getListaPrzedmiotow()) {
                if (pesel==przedmiot.getWykladowca().getPesel());
                System.out.println("Prowadzi zajęci z przedmiotu: "+przedmiot.getNazwa()+" z grupy przedmiotow "+grupaPrzedmiotow.getIdentyfikator());
                znalezionoProwadzacego=true;

            }

        }
        if (!znalezionoProwadzacego){
            System.out.println("Prowadzacy o podanym peselu nie został odnaleziony");
        }
    }

    public void pokazPrzedmiotyStudentaPoPeselu(int pesel) {
        boolean znalezionoStudenta = false;
        System.out.println("Student: " + znajdzStudentaIZwrocDane(pesel));
        System.out.println("");
        for (GrupaPrzedmiotow grupaPrzedmiotow : listaGrupPrzedmiotow) {
            for (Przedmiot przedmiot : grupaPrzedmiotow.getListaPrzedmiotow()) {
                for (Student student : przedmiot.getListaStudentow()) {
                    if (pesel == student.getPesel()) {
                        System.out.println("Uczeszcza na zajecia:" + przedmiot.getNazwa() + " z grupy przedmiotow: " + grupaPrzedmiotow.getIdentyfikator());
                        System.out.println("Zajecia sa prowadzone przez: " + przedmiot.getWykladowca().toString());
                        znalezionoStudenta = true;
                    }
                }
            }

        }
        if (!znalezionoStudenta) {
            System.out.println("Student o podanym peselu nie został znaleziony!");
        }
    }


    public int getNpPokoju() {
        return npPokoju;
    }

    public void setNpPokoju(int npPokoju) {
        this.npPokoju = npPokoju;
    }

    public List<GrupaPrzedmiotow> getListaGrupPrzedmiotow() {
        return listaGrupPrzedmiotow;
    }

    public void setListaGrupPrzedmiotow(List<GrupaPrzedmiotow> listaGrupPrzedmiotow) {
        this.listaGrupPrzedmiotow = listaGrupPrzedmiotow;
    }
}
