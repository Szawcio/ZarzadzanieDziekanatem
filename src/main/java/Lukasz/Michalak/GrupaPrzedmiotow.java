package Lukasz.Michalak;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GrupaPrzedmiotow {

    private String identyfikator;
    List<Przedmiot> listaPrzedmiotow;

    public GrupaPrzedmiotow(String identyfikator) {
        this.identyfikator = identyfikator;
        this.listaPrzedmiotow = new ArrayList<Przedmiot>();
    }

    public void dodajPrzedmiotDoListy(Przedmiot przedmiot){
        listaPrzedmiotow.add(przedmiot);
    }

    public void usunPrzedmiotZListy(Przedmiot przedmiot){
        listaPrzedmiotow.remove(przedmiot);

    }

    public void usunPrzedmiotPoNazwie(String nazwa){
        boolean znaleziony=false;
        Iterator<Przedmiot> iterator = listaPrzedmiotow.iterator();
        while (iterator.hasNext()){
            Przedmiot przedmiot = iterator.next();
            if(nazwa.equals(przedmiot.getNazwa())){
                System.out.println("Przedmiot "+ przedmiot.getNazwa()+" został usuniety");
                iterator.remove();
            }

        }
        if(znaleziony==false){
            System.out.println("Nie znaleziono przedmiotu o podanej nazwie");
        }
    }


    @Override
    public String toString() {
        return "W grupie przedmiotow o identyfikatorze "
                + identyfikator +
                " jest aktualnie " + listaPrzedmiotow.size() + " przedmiotow";

    }

    public String getIdentyfikator() {
        return identyfikator;
    }

    public void setIdentyfikator(String identyfikator) {
        this.identyfikator = identyfikator;
    }

    public List<Przedmiot> getListaPrzedmiotow() {
        return listaPrzedmiotow;
    }

    public void setListaPrzedmiotow(List<Przedmiot> listaPrzedmiotow) {
        this.listaPrzedmiotow = listaPrzedmiotow;
    }
}
