package Lukasz.Michalak;


public class Wykladowca {
    private String imie;
    private String nazwisko;
    private int pesel;
    private String zaklad;
    private String stanowisko;
    private String stopien;

    public Wykladowca(String imie, String nazwisko, int pesel, String zaklad, String stanowisko, String stopien) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.pesel = pesel;
        this.zaklad = zaklad;
        this.stanowisko = stanowisko;
        this.stopien = stopien;
    }


    @Override
    public String toString() {
        return "Wykladowcą jest: " +
                stopien +
                " " + imie +
                " " + nazwisko +
                ", pesel: " + pesel +
                ", z zakładu: " + zaklad +
                ", stanowisko: " + stanowisko;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public int getPesel() {
        return pesel;
    }

    public void setPesel(int pesel) {
        this.pesel = pesel;
    }

    public String getZaklad() {
        return zaklad;
    }

    public void setZaklad(String zaklad) {
        this.zaklad = zaklad;
    }

    public String getStanowisko() {
        return stanowisko;
    }

    public void setStanowisko(String stanowisko) {
        this.stanowisko = stanowisko;
    }

    public String getStopien() {
        return stopien;
    }

    public void setStopien(String stopien) {
        this.stopien = stopien;
    }
}
