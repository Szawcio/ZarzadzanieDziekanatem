package Lukasz.Michalak;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Przedmiot {

    private String nazwa;
    private int punktyECTS;

    private Wykladowca wykladowca;
    private List<Student> listaStudentow;

    public Przedmiot(String nazwa, int punktyECTS) {
        this.nazwa = nazwa;
        this.punktyECTS = punktyECTS;
        this.listaStudentow = new ArrayList<Student>();
    }

    public void dodajStudenta(Student student) {
        listaStudentow.add(student);
    }

    public void usunStudenta(Student student) {
        System.out.println("Usunięto studenta: " + student.getImie() + " " + student.getNazwisko() + " o peselu: " + student.getPesel() + " z przedmiotu " + nazwa);
        listaStudentow.remove(student);
    }

    public void usunStudentaPoPeselu(int pesel) {
        boolean znaleziony = false;
        Iterator<Student> iterator = listaStudentow.iterator();
        while (iterator.hasNext()) {
            Student student = iterator.next();
            if (pesel == student.getPesel()) {
                System.out.println("Usunięto studenta: " + student.getImie() + " " + student.getNazwisko() + " o peselu: " + student.getPesel() + " z przedmiotu " + nazwa);
                iterator.remove();
                znaleziony = true;
            }
        }
        if (!znaleziony) {
            System.out.println("Nie znaleziono studenta o podanym peselu");
        }
    }

    public void usunWykladowce(){
        this.wykladowca=null;
    }

    @Override
    public String toString() {
        return "Przedmiot: "
                + nazwa +
                ", ECTS za przedmiot " + punktyECTS +
                ", Wykładowca: " + wykladowca.toString() +
                ", "+ listaStudentow.size() +" studentow na liście przedmiotu";

    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public int getPunktyECTS() {
        return punktyECTS;
    }

    public void setPunktyECTS(int punktyECTS) {
        this.punktyECTS = punktyECTS;
    }

    public Wykladowca getWykladowca() {
        return wykladowca;
    }

    public void setWykladowca(Wykladowca wykladowca) {
        this.wykladowca = wykladowca;
    }

    public List<Student> getListaStudentow() {
        return listaStudentow;
    }

    public void setListaStudentow(List<Student> listaStudentow) {
        this.listaStudentow = listaStudentow;
    }
}
