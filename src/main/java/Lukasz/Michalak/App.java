package Lukasz.Michalak;


public class App {
    public static void main(String[] args) {
        System.out.println("Hello World!");

        Przedmiot p1 = new Przedmiot("matma", 2);

        Student s1 = new Student("lukasz", "michalak", 123, 3);
        Student s2 = new Student("lukasz", "michalak", 124, 3);
        Student s3 = new Student("lukasz", "michalak", 125, 3);
        Student s4 = new Student("lukasz", "michalak", 126, 3);
        Student s5 = new Student("lukasz", "michalak", 127, 3);

        p1.dodajStudenta(s1);
        p1.dodajStudenta(s2);
        p1.dodajStudenta(s3);
        p1.dodajStudenta(s4);
        p1.dodajStudenta(s5);

        p1.usunStudentaPoPeselu(123);

        Wykladowca w1 = new Wykladowca("Jan", "Kowalski", 123, "BOiKD", "wykladowca", "dr");
        System.out.println(w1.toString());
        System.out.println();
        System.out.println(s4.toString());


        p1.setWykladowca(w1);
        System.out.println(p1.toString());


    }
}
